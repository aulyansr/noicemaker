$(document).ready(function () {
    var swiper = new Swiper('.topic-slider', {

        breakpoints: {
            1200: {
                spaceBetween: 20,
                centeredSlides: true,
                loop: false,
                slidesPerView: 3,
                grid: {
                    rows: 2,
                    fill: "row",
                    centeredSlides: true,
                },
            },
            // when window width is >= 320px
            320: {
                centeredSlides: true,
                slidesPerView: 1,
                spaceBetween: 20,

            },
            // when window width is >= 480px
            480: {
                centeredSlides: true,
                slidesPerView: 1,
                spaceBetween: 20,

            },
            // when window width is >= 640px

        },
    });

    var swiper = new Swiper('.partner-slider', {
        spaceBetween: 100,
        autoHeight: true, //enable auto height
        centeredSlides: true,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        autoplay: {
            speeds: 2000,
            delay: 4000,
        },
        speed: 1000,
        breakpoints: {
            1590: {
                slidesPerView: 6,
            },
            // when window width is >= 320px
            320: {
                centeredSlides: true,
                slidesPerView: 2,
                spaceBetween: 20,

            },
            // when window width is >= 480px
            480: {
                centeredSlides: true,
                slidesPerView: 2,
                spaceBetween: 20,

            },
            // when window width is >= 640px
            640: {
                slidesPerView: 6,
            }
        }
    });
    var swiper = new Swiper('.noice-space-slider', {
        spaceBetween: 8,
        centeredSlides: true,
        loop: false,
        speed: 1000,
        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: "auto",
                spaceBetween: 20,
                centeredSlides: false,

            },
            // when window width is >= 480px
            480: {
                slidesPerView: 3,
                spaceBetween: 30,
                centeredSlides: false,
            },
            // when window width is >= 640px
            640: {
                slidesPerView: 3,
                spaceBetween: 20,
                centeredSlides: false,
            }
        }
    });

    var swiper = new Swiper('.event-slider', {
        spaceBetween: 10,

        autoplay: {
            speeds: 2000,
            delay: 4000,
        },
        speed: 1000,
        breakpoints: {
            1590: {
                slidesPerView: 4,
                loop: false,
            },
            991: {
                slidesPerView: 4,
                loop: false,
            },
            767: {
                slidesPerView: 2,
                loop: true,
            },
            575: {
                slidesPerView: 2,
                loop: true,
            },
            // when window width is >= 320px
            320: {
                slidesPerView: "auto",
                spaceBetween: 20,
                centeredSlides: false,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            },
        }
    });

    var swiper = new Swiper('.article-slider', {
        spaceBetween: 20,

        autoplay: {
            speeds: 2000,
            delay: 4000,
        },
        speed: 1000,
        breakpoints: {
            1590: {
                slidesPerView: 4,
                loop: false,
            },
            991: {
                slidesPerView: 4,
                loop: false,
            },
            767: {
                slidesPerView: 2,
                loop: true,
            },
            575: {
                slidesPerView: 2,
                loop: true,
            },
            // when window width is >= 320px
            320: {
                slidesPerView: "auto",
                spaceBetween: 20,
                centeredSlides: false,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            },
        }
    });

    $('select').niceSelect();

    var time = 15
    setInterval(function () {
        var seconds = time % 60;

        document.getElementById("time").innerHTML = seconds;
        time--;
        if (time == 0) {
            window.location.href = "index.html";
        }
    }, 1000);
});